provider "aws" {
  region = "us-east-2"
}
terraform {

  backend "s3" {
    bucket = "eks-bucket-gochi"
    key = "s3-state.tfstate"
    region = "us-east-2"
    encrypt = true
   }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}